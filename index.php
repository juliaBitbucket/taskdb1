<?php
require_once 'autoloader.php';

$db = new PDORepository();

$option = array();

$sql = "SELECT * FROM books WHERE 1=1";

if (!empty($_GET)) {
    foreach ($_GET as $key => $value) {
        $sql .= " AND `{$key}` LIKE :{$key}";
        $option[$key] = "%".$value."%";
    }
}

$books = $db->getData($sql, $option);

?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset=utf-8">
        <title>Библиотека успешного человек</title>
        <style>
            form {
                margin-bottom: 1em;
            }

            form {
                display: block;
                margin-top: 0em;
            }
            table {
                border-spacing: 0;
                border-collapse: collapse;
            }

            table td, table th {
                border: 1px solid #ccc;
                padding: 5px;
            }

            table th {
                background: #eee;
            }
        </style>
    </head>
    <body>
        <h1>Библиотека успешного человека</h1>

        <form method="GET">
            <input type="text" name="isbn" placeholder="ISBN" value="<?php echo $_GET['isbn']; ?>" />
            <input type="text" name="name" placeholder="Название книги" value="<?php echo $_GET['name']; ?>" />
            <input type="text" name="author" placeholder="Автор книги" value="<?php echo $_GET['author']; ?>" />
            <input type="submit" value="Поиск" />
        </form>

        <table>
            <tr>
                <th>Название</th>
                <th>Автор</th>
                <th>Год выпуска</th>
                <th>Жанр</th>
                <th>ISBN</th>
            </tr>
            <?php if (!empty($books)):
                    foreach ($books as $book): ?>
            <tr>
                <td><?php echo $book['name'];?></td>
                <td><?php echo $book['author'];?></td>
                <td><?php echo $book['year'];?></td>
                <td><?php echo $book['genre'];?></td>
                <td><?php echo $book['isbn'];?></td>
            </tr>
            <?php endforeach;endif;?>
        </table>
    </body>
</html>