<?php

class PDORepository{
    const USERNAME="root";
    const PASSWORD="root";
    const HOST="localhost";
    const DB="books";

    private function getConnection(){
        $username = self::USERNAME;
        $password = self::PASSWORD;
        $host = self::HOST;
        $db = self::DB;
        $connection = new PDO("mysql:dbname=$db;host=$host;charset=utf8", $username, $password);
        return $connection;
    }

    public function getData($sql, $args){
        $connection = $this->getConnection();
        $stmt = $connection->prepare($sql);
        $stmt->execute($args);
        $data = array();
        while ($row = $stmt->fetch()) {
            $data[] = $row;
        }
        return $data;
    }
}

?>